package ru.nsu.ccfit.s.udod;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.util.ArrayList;


public class ClientPage extends Page {

    public ClientPage(MyModel tableModel){
        super(tableModel);

        JPanel insertPanel= new JPanel();
        insertPanel.setLayout(new GridLayout(5,2, 1 , 80));
        insertPanel.add(new JLabel("name "));
        JTextField nameField = new JTextField();
        insertPanel.add(nameField);
        insertPanel.add(new JLabel("lastname "));
        JTextField lastnameField = new JTextField();
        insertPanel.add(lastnameField );
        insertPanel.add(new JLabel("phoneNumb "));
        JTextField phoneNumber = new JTextField();
        insertPanel.add(phoneNumber);

        JButton insertButton = new JButton("Insert");
        insertButton.addActionListener(e -> {
            String result;
            Date date;
            ArrayList list=new ArrayList();
            if(nameField.getText()==null|| lastnameField.getText()==null||phoneNumber.getText()==null)
                result="заполнены не все поля";
            else{
                list.add( "\'"+0+ "\'");
                list.add("\'"+nameField.getText()+ "\'");
                list.add("\'"+lastnameField.getText()+ "\'");
                list.add("\'"+phoneNumber.getText()+ "\'");
                date=new Date(System.currentTimeMillis());
                list.add("\'"+date.toLocalDate().getDayOfMonth()+"."+date.toLocalDate().getMonth()+"."+date.toLocalDate().getYear()+ "\'");
                result=tableModel.addRow(list);
            }
            showAnswer(result);
        });
        insertPanel.add(new JLabel());
        insertPanel.add(insertButton);

        setLayout(new BorderLayout());
        add(new JScrollPane(table),BorderLayout.EAST);
        add(insertPanel,BorderLayout.CENTER);
    }

}
