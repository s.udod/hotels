package ru.nsu.ccfit.s.udod;

import javax.swing.*;

public class Main extends JFrame {
    public Main() {
        super("Hotel Database");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        String url = "jdbc:oracle:thin:@" + "84.237.50.81:1521" + ":";
        String userName = "S.UDOD";
        String pass = "udod";
        DataWorker dataWorker =new DataWorker(url,userName,pass);

        new Menu().createPanelUI(getContentPane(),dataWorker);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame.setDefaultLookAndFeelDecorated(true);
                new Main();
            }
        });

    }

}

