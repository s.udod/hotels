package ru.nsu.ccfit.s.udod;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

public class RoomPage extends Page {

    protected Action aDeleteRow = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (e.getSource() == table) {

                int r = table.getSelectedRow();
                if (r >= 0) {
                    tableModel.removeRow(r,"\"Комнаты\"");

                }
            }
        }
    };

    public RoomPage(MyModel tableModel) {
        super(tableModel);
        JPanel insertPanel = new JPanel();
        insertPanel.setLayout(new GridLayout(10, 1, 0, 20));

        tableModel.setSubstitution(2, "SELECT \"id\",\"name\"\n" + "FROM \"Корпуса\"");
        tableModel.setSubstitution(3, "SELECT \"id\", \"name\"\n" + "FROM \"Категории\" ");
        tableModel.falseChange();

        Object[] corpuses = tableModel.getSubstitution(2).values().toArray();
        JComboBox corpusCombox = new JComboBox(corpuses);

        Object[] categories = tableModel.getSubstitution(3).values().toArray();
        JComboBox categoriesCombox = new JComboBox(categories);

        JTextField number = new JTextField();
        JSpinner spinnerFloor = new JSpinner();

        insertPanel.add(new JLabel("corpus "));
        insertPanel.add(corpusCombox);
        insertPanel.add(new JLabel("number"));
        insertPanel.add(number);
        insertPanel.add(new JLabel("category"));
        insertPanel.add(categoriesCombox);
        insertPanel.add(new JLabel("floor"));
        insertPanel.add(spinnerFloor);
        tableModel.falseChange();
        table.getActionMap().remove("del");
        table.getActionMap().put("del", aDeleteRow);


        JButton insertButton = new JButton("Insert");
        insertButton.addActionListener(e -> {
            String result;
            Date date;
            ArrayList list = new ArrayList();
            if (number.getText() == null)
                result = "заполнены не все поля";
            else {
                result=tableModel.addRow(
                        "DECLARE\n" +
                                "mynumber INT := " + number.getText() + ";\n" +
                                "corpusId INT:= " + tableModel.getSubstitution(2).keySet().toArray()[corpusCombox.getSelectedIndex()] + ";\n" +
                                "category INT:= " + tableModel.getSubstitution(3).keySet().toArray()[categoriesCombox.getSelectedIndex()] + ";\n" +
                                "floor INT := " + spinnerFloor.getValue() + ";\n"+
                                "BEGIN\n" +
                                "add_room(mynumber , corpusId, category, floor);\n" +
                                "END;\n");

               showAnswer(result);
            }

        });
        insertPanel.add(new JLabel());
        insertPanel.add(insertButton);

        setLayout(new BorderLayout());
        add(insertPanel, BorderLayout.WEST);
        add(new JLabel("    "),BorderLayout.CENTER);
        add(new JScrollPane(table), BorderLayout.EAST);

    }


}
