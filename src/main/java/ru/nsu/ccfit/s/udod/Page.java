package ru.nsu.ccfit.s.udod;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class Page extends JPanel {
    protected JTable table;
    protected MyModel tableModel;

    public Page(MyModel tableModel) {
        this.tableModel = tableModel;
        this.table = new JTable(tableModel);

        table.getInputMap(JComponent.WHEN_FOCUSED).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "del");
        table.getActionMap().put("del", actDeleteRow);
    }

    protected Action actDeleteRow = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (e.getSource() == table) {

                int r = table.getSelectedRow();
                if (r >= 0) {
                    tableModel.removeRow(r);
                }
            }
        }
    };

    protected void showAnswer(String result){
        if (result==null) {
            JOptionPane.showMessageDialog(this, "Данные успешно добавлены!");
        } else {
            JOptionPane.showMessageDialog(this, "Введены неверные данные\n" + result, "Ошибка", JOptionPane.ERROR_MESSAGE);
        }

    }


}
