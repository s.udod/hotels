package ru.nsu.ccfit.s.udod;

import java.io.Closeable;
import java.io.IOException;
import java.sql.*;
import java.util.*;

public class DataWorker implements Closeable {
    private Connection conn = null;
    private Statement stmt = null;

    public DataWorker(String url, String userName, String pass) {

        TimeZone timeZone = TimeZone.getTimeZone("GMT+7");
        TimeZone.setDefault(timeZone);
        Locale.setDefault(Locale.ENGLISH);

        try {
            conn = DriverManager.getConnection(url, userName, pass);
            stmt = conn.createStatement();
            if (conn == null) {
                System.out.println("Нет соединения с БД!");
                System.exit(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet getData(String request) {

        try {
            ResultSet rs = stmt.executeQuery(request);
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    public String getAnswer(String request) {

        try {

            stmt.execute(request);
            return null;
        } catch (SQLException e) {
            return e.getMessage();
        }
    }
    public boolean update(String tablename, String column, String value, String key , String keyValue ){

        try {
            return stmt.executeUpdate("UPDATE " +tablename+ " SET "+column+"="+value+ " WHERE "+key+"="+keyValue)>0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    public String insert(String table, List<Object> values) {
        String insert = "INSERT INTO " + table + " VALUES "+"(";
        for (int i = 0; i < values.size(); i++) {
            insert +=values.get(i);
            if (i != values.size() - 1) {
                insert += ", ";
            }
        }
        insert += ")";
        try {
            stmt.executeUpdate(insert);
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    public ResultSet getAllFromTable(String tablename) {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT * FROM " + tablename);
        } catch (SQLException e) {
           e.printStackTrace();
        }
        return rs;
    }

    public void delete(String tableName,String key , String keyValue){
        try {
            ResultSet rs = stmt.executeQuery("DELETE FROM " + tableName+" WHERE"+key+"="+keyValue);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Map parseData(ResultSet rs ){
        Map<Integer,Object> data = new HashMap<>();
        try {
        while (true) {
            if (!rs.next()) break;
            String rsData=new String();

            for (int i = 1; i < rs.getMetaData().getColumnCount(); i++) {
               rsData=rsData+" "+(rs.getString(i + 1));
            }
            data.put(rs.getInt(1),rsData);
        }
        } catch (SQLException e) {
                e.printStackTrace();
        }
        return data;
    }


    @Override
    public void close() throws IOException {
        try {
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
