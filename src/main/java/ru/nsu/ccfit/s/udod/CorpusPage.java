package ru.nsu.ccfit.s.udod;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

public class CorpusPage extends Page {
    private JTextField nameField;
    private JSpinner spinnerFloor;
    private JSpinner spinnerRoom;
    private JButton insertButton;

    public CorpusPage(MyModel tableModel) {
        super(tableModel);
        GridBagConstraints gbc;
        setLayout(new GridBagLayout());

        nameField = new JTextField();
        gbc = getGridBagConstraints(1, 1, 1, 5, 1, 1);
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(nameField, gbc);

        final JLabel label1 = new JLabel();
        label1.setText("Corpus name");
        gbc = getGridBagConstraints(0, 1, 1, 1, 1, 1);
        gbc.anchor = GridBagConstraints.WEST;
        add(label1, gbc);

        final JLabel label2 = new JLabel();
        label2.setText("Class");
        gbc = getGridBagConstraints(0, 2, 1, 1, 1, 1);
        gbc.anchor = GridBagConstraints.WEST;
        add(label2, gbc);

        String[] items = {"1", "2", "3", "4", "5"};
        final JComboBox clas = new JComboBox(items);
        gbc = getGridBagConstraints(1, 2, 1, 1, 1, 1);
        gbc.anchor = GridBagConstraints.WEST;
        add(clas, gbc);


        final JLabel label3 = new JLabel();
        label3.setText("Floor Count");
        gbc = getGridBagConstraints(0, 3, 1, 1, 1, 1);
        gbc.anchor = GridBagConstraints.WEST;
        add(label3, gbc);

        spinnerFloor = new JSpinner();
        gbc = getGridBagConstraints(1, 3, 1, 2, 1, 1);
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(spinnerFloor, gbc);

        final JLabel label4 = new JLabel();
        label4.setText("Room on the floor");
        gbc = getGridBagConstraints(0, 4, 1, 1, 1, 1);
        gbc.anchor = GridBagConstraints.WEST;
        add(label4, gbc);

        spinnerRoom = new JSpinner();
        gbc = getGridBagConstraints(1, 4, 1, 2, 1, 1);
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(spinnerRoom, gbc);

        insertButton = new JButton();
        insertButton.addActionListener(e -> {
            String result;
            ArrayList list = new ArrayList();
            if (nameField.getText() == null || clas.getSelectedIndex() == -1)
                result = "неверно заполнены поля";
            else {
                list.add("\'"+0+ "\'");
                list.add("\'"+nameField.getText()+ "\'");
                list.add("\'"+items[clas.getSelectedIndex()]+ "\'");
                list.add("\'"+spinnerFloor.getValue()+ "\'");
                list.add("\'"+spinnerRoom.getValue()+ "\'");
                result = tableModel.addRow(list);
            }
            showAnswer(result);
        });

        insertButton.setText("Insert");
        gbc = getGridBagConstraints(1, 5, 1, 1, 1, 1);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(insertButton, gbc);

        gbc = getGridBagConstraints(6, 1, 5, 1, 1, 1);
        gbc.fill = GridBagConstraints.BOTH;

        add(new JScrollPane(table), gbc);
    }

    private GridBagConstraints getGridBagConstraints(int x, int y, int height, int width, double weightx, double weighty) {
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.gridheight = height;
        gbc.gridwidth = width;
        gbc.weightx = weightx;
        gbc.weighty = weighty;
        return gbc;
    }
}
