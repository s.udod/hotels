package ru.nsu.ccfit.s.udod;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class AccommondationMenu extends JPanel implements ItemListener  {
    private static JPanel cards;
    final static String FILTRPANEL = "Фильтр";
    final static String INFOPANEL = "Вся информация";

    public AccommondationMenu(DataWorker dataWorker) {
        setLayout(new BorderLayout());

        // Создание компонента с выпадающим списком
        JComboBox<String> combobox = new JComboBox<String>(
                new String[] { INFOPANEL , FILTRPANEL});
        combobox.setEditable    (false);
        combobox.addItemListener(this );

        JPanel cbPanel = new JPanel();
        cbPanel.add(combobox);
        //создаем вкладки


        MyModel accomdModel=new MyModel("\"Заселения\"",dataWorker);
        AccommodationPage accomdPage=new AccommodationPage(accomdModel,dataWorker);

        FullInformation fullInformation=new FullInformation(dataWorker);
        // Создаем панель с менеджером расположения CardLayout
        cards = new JPanel(new CardLayout());
        // Добавляем вкладки


        cards.add(accomdPage,INFOPANEL);
        cards.add(fullInformation,FILTRPANEL);
        add(cbPanel, BorderLayout.PAGE_START);
        add(cards  , BorderLayout.CENTER );
    }
    @Override
    public void itemStateChanged(ItemEvent e) {
        CardLayout layout = (CardLayout)(cards.getLayout());
        layout.show(cards, (String)e.getItem());
    }
}
