package ru.nsu.ccfit.s.udod;

import javax.swing.*;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

public class FullInformation extends JPanel {
    public FullInformation(DataWorker dataWorker) {
        String request = "SELECT \"roomId\",\"name\",\"number\"\n" +
                "FROM \"Комнаты\" JOIN \"Корпуса\" ON(\"Комнаты\".\"corpusId\"=\"Корпуса\".\"id\")";

        JPanel insertPanel = new JPanel();
        Map substitution;
        synchronized (dataWorker) {
            substitution = dataWorker.parseData(dataWorker.getData(request));
        }
        JTable table = new JTable();

        insertPanel.setLayout(new GridLayout(5, 2, 1, 70));

        MaskFormatter mf = null;
        try {
            mf = new MaskFormatter("##.##.####");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mf.setPlaceholderCharacter('_');
        JFormattedTextField startD = new JFormattedTextField(mf);
        JFormattedTextField endD = new JFormattedTextField(mf);


        Object[] rooms = substitution.values().toArray();
        JComboBox roomCombox = new JComboBox(rooms);
        JTextField number = new JTextField();


        insertPanel.add(new JLabel("room "));
        insertPanel.add(roomCombox);
        insertPanel.add(new JLabel("startDate"));
        insertPanel.add(startD);
        insertPanel.add(new JLabel("endDate"));
        insertPanel.add(endD);

        JButton findButton = new JButton("Find");
        findButton.addActionListener(e -> {
            String result;
            Date date;
            ArrayList list = new ArrayList();
            if (startD.getText() == null || endD.getText() == null)
                result = "заполнены не все поля";
            else {
                synchronized (dataWorker) {
                    MyModel tableModel = new MyModel(dataWorker, "SELECT \"Клиенты\".\"name\",\"Клиенты\".\"lastname\", \"Заселения\".\"startDate\", \"Заселения\".\"endDate\"\n" +
                            "FROM \"Заселения\" " +
                            "LEFT JOIN \"Клиенты\" ON (\"Клиенты\".\"id\"=\"Заселения\".\"clientID\")\n" +
                            "WHERE \"Заселения\".\"roomId\"= \'" + substitution.keySet().toArray()[roomCombox.getSelectedIndex()] + "\'\n" +
                            "AND \"Заселения\".\"startDate\" BETWEEN " + "to_date(" + "\'" + startD.getText() + "\'" + ", \'DD.MM.YYYY\')" +
                            " AND " + "to_date(" + "\'" + endD.getText() + "\'" + ", \'DD.MM.YYYY\')" + "\n");
                    tableModel.falseChange();
                    table.setModel(tableModel);
                    result = null;
                }
            }
            showAnswer(result);
        });
        insertPanel.add(new JLabel());
        insertPanel.add(findButton);
        setLayout(new BorderLayout());
        add(new JScrollPane(table), BorderLayout.EAST);
        add(insertPanel, BorderLayout.CENTER);
    }
    private void showAnswer(String result){
        if (result==null) {
            JOptionPane.showMessageDialog(this, " Успешно !");
        } else {
            JOptionPane.showMessageDialog(this,  result, "Ошибка", JOptionPane.ERROR_MESSAGE);
        }

    }
}

