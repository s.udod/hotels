package ru.nsu.ccfit.s.udod;

import javax.swing.*;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;


public class AccommodationPage extends Page {

    public AccommodationPage(MyModel tableModel, DataWorker dataWorker) {
        super(tableModel);
        JLabel countRooms = new JLabel();
        JPanel insertPanel = new JPanel();
        insertPanel.setLayout(new GridLayout(7, 2, 1, 45));
        insertPanel.add(new JLabel("number of room available now:"));
        insertPanel.add(countRooms);
        tableModel.setSubstitution(1, "SELECT \"id\",\"name\",\"lastname\"\n" + "FROM \"Клиенты\"");
        tableModel.setSubstitution(2, "SELECT \"roomId\",\"name\",\"number\"\n" +
                "FROM \"Комнаты\" JOIN \"Корпуса\" ON(\"Комнаты\".\"corpusId\"=\"Корпуса\".\"id\")");
        tableModel.deleteColumn(tableModel.getColumnCount() - 1);
        tableModel.falseChange();

        Object[] clients = tableModel.getSubstitution(1).values().toArray();
        JComboBox clientCombox = new JComboBox(clients);

        Object[] rooms = tableModel.getSubstitution(2).values().toArray();
        JComboBox roomCombox = new JComboBox(rooms);

        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    synchronized (dataWorker) {
                        countRooms.setText("    "+dataWorker.parseData(dataWorker.getData(
                                "SELECT COUNT(\"Комнаты\".\"roomId\") as CountFreeRoom\n" +
                                        "FROM \"Комнаты\"\n" +
                                        "WHERE \"Комнаты\".\"roomId\" NOT IN \n" +
                                        "(SELECT \"Заселения\".\"roomId\"\n" +
                                        "FROM \"Заселения\"\n" +
                                        "WHERE CURRENT_DATE BETWEEN \"Заселения\".\"startDate\" AND \"Заселения\".\"endDate\"\n" +
                                        "GROUP BY \"Заселения\".\"roomId\")\n")).keySet().toArray()[0].toString());
                    }

                    try {
                        Thread.sleep(30000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();


        insertPanel.add(new JLabel("client"));



        MaskFormatter mf = null;
        try {
            mf = new MaskFormatter("##.##.####");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mf.setPlaceholderCharacter('_');
        JFormattedTextField startD = new JFormattedTextField(mf);
        JFormattedTextField endD = new JFormattedTextField(mf);
        insertPanel.add(clientCombox);
        insertPanel.add(new JLabel("room"));
        insertPanel.add(roomCombox);
        insertPanel.add(new JLabel("startDate"));
        insertPanel.add(startD);
        insertPanel.add(new JLabel("endDate"));
        insertPanel.add(endD);
        JButton insertButton = new JButton("Insert");
        insertButton.addActionListener(e -> {
            String result;
            Date date;
            ArrayList list = new ArrayList();
            if (startD.getText() == null || endD.getText() == null)
                result = "заполнены не все поля";
            else {
                list.add("\'" + 0 + "\'");
                list.add("\'" + tableModel.getSubstitution(1).keySet().toArray()[clientCombox.getSelectedIndex()] + "\'");
                list.add("\'" + tableModel.getSubstitution(2).keySet().toArray()[roomCombox.getSelectedIndex()] + "\'");
                list.add("to_date(" + "\'" + startD.getText() + "\'" + ", \'DD.MM.YYYY\')");
                list.add("to_date(" + "\'" + endD.getText() + "\'" + ", \'DD.MM.YYYY\')");
                list.add("\'" + "\'");
                result = tableModel.addRow(list);
            }
            showAnswer(result);
        });
        insertPanel.add(new JLabel());
        insertPanel.add(insertButton);
        setLayout(new BorderLayout());
        add(new JScrollPane(table), BorderLayout.EAST);
        add(insertPanel, BorderLayout.CENTER);
    }
}
