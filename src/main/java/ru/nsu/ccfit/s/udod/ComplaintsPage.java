package ru.nsu.ccfit.s.udod;

import javax.swing.*;
import java.awt.*;
import java.sql.Date;
import java.util.ArrayList;

public class ComplaintsPage extends Page{
    public ComplaintsPage(MyModel tableModel) {
        super(tableModel);
        JPanel insertPanel= new JPanel();
        insertPanel.setLayout(new GridLayout(1,6, 1 , 1));
        insertPanel.add(new JLabel("account Id "));
        JTextField account = new JTextField();
        insertPanel.add(account);
        insertPanel.add(new JLabel("complaint"));
        JTextField complaint = new JTextField();
        insertPanel.add(complaint );
        tableModel.falseChange();
        table.getActionMap().remove("del");

        JButton insertButton = new JButton("Insert");
        insertButton.addActionListener(e -> {
            String result;
            Date date;
            ArrayList list=new ArrayList();
            if(account.getText()==null|| complaint.getText()==null)
                result="заполнены не все поля";
            else{
                result=tableModel.updateRow("\"Заселения\"","\"complaint\"","\'"+complaint.getText()+ "\'","\"id\"","\'"+account.getText()+ "\'");
               showAnswer(result);
            }

        });
        insertPanel.add(new JLabel());
        insertPanel.add(insertButton);

        setLayout(new BorderLayout());
        add(insertPanel,BorderLayout.NORTH);
        add(new JLabel(" "),BorderLayout.CENTER);
        add(new JScrollPane(table),BorderLayout.SOUTH);


    }
}
