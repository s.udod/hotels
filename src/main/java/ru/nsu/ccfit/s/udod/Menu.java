package ru.nsu.ccfit.s.udod;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class Menu implements ItemListener {
    private static JPanel cards;
    final static String CORPUSPANEL = "Корпуса";
    final static String CLIENTPANEL = "Клиенты";
    final static String ACCOMMPANEL = "Заселения";
    final static String COMPPANEL = "Жалобы";
    final static String ROOMPPANEL = "Комнаты";
    final static String FULLINFO = "Подробная информация ";

    public void createPanelUI(Container container, DataWorker dataWorker)
    {
        // Создание компонента с выпадающим списком
        JComboBox<String> combobox = new JComboBox<String>(
                new String[] { CORPUSPANEL , CLIENTPANEL,ACCOMMPANEL,COMPPANEL,ROOMPPANEL});
        combobox.setEditable    (false);
        combobox.addItemListener(this );

        JPanel cbPanel = new JPanel();
        cbPanel.add(combobox);
        //создаем вкладки
        MyModel corpuseModel=new MyModel("\"Корпуса\"",dataWorker);
        CorpusPage corpusPage=new CorpusPage(corpuseModel);

        MyModel clientModel=new MyModel("\"Клиенты\"",dataWorker);
        ClientPage clientPage=new ClientPage(clientModel);

        MyModel accomdModel=new MyModel("\"Заселения\"",dataWorker);
        AccommodationPage accomdPage=new AccommodationPage(accomdModel,dataWorker);

        MyModel complaintModel=new MyModel(dataWorker,
                "SELECT \"Клиенты\".\"name\",\"lastname\",\"Корпуса\".\"name\" AS \"Corpus\",\"Комнаты\".\"number\",\"startDate\",\"endDate\",\"complaint\"\n" +
                        "FROM \"Заселения\" LEFT JOIN \"Клиенты\" ON (\"Заселения\".\"clientID\"=\"Клиенты\".\"id\")\n" +
                        "LEFT JOIN \"Комнаты\" ON (\"Заселения\".\"roomId\"=\"Комнаты\".\"roomId\")\n" +
                        "LEFT JOIN \"Корпуса\" ON (\"Комнаты\".\"corpusId\"=\"Корпуса\".\"id\")\n" +
                        "WHERE \"Заселения\".\"complaint\" IS NOT NULL\n");
        ComplaintsPage complaintPage= new ComplaintsPage(complaintModel);

        MyModel roomModel=new MyModel(dataWorker,"SELECT *\n" + "FROM \"Комнаты\" JOIN \"Инф о комнатах\" USING (\"roomId\")");
        RoomPage roomPage=new RoomPage(roomModel);

        //FullInformation fullInformation=new FullInformation(dataWorker);
        // Создаем панель с менеджером расположения CardLayout
        cards = new JPanel(new CardLayout());
        // Добавляем вкладки
        cards.add(corpusPage, CORPUSPANEL);
        cards.add(clientPage,CLIENTPANEL);
        cards.add(new AccommondationMenu(dataWorker),ACCOMMPANEL);
        cards.add(complaintPage,COMPPANEL);
        cards.add(roomPage,ROOMPPANEL);
       // cards.add(fullInformation,FULLINFO);
        container.add(cbPanel, BorderLayout.PAGE_START);
        container.add(cards  , BorderLayout.CENTER );
    }
    @Override
    public void itemStateChanged(ItemEvent e) {
        CardLayout layout = (CardLayout)(cards.getLayout());
        layout.show(cards, (String)e.getItem());
    }
}
