package ru.nsu.ccfit.s.udod;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

public class MyModel extends AbstractTableModel {
    private List<List<Object>> data;
    private List<String> columnName = new ArrayList<>();
    private List<Class<?>> columnTypes = new ArrayList<>();
    private Set<TableModelListener> listeners = new HashSet<>();
    private DataWorker dataWorker;
    private String tableName;
    private Map<Integer, Map<Integer, Object>> substitution;
    private boolean flag;
    private String request;


    public MyModel(DataWorker dataWorker,String request) {
        try {
            this.substitution = new HashMap<>();
            this.dataWorker = dataWorker;
            this.request=request;
            synchronized (dataWorker) {
                setDataSource(dataWorker.getData(request));
            }
            flag = true;

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();

        }
    }

    public MyModel(String tableName, DataWorker dataWorker) {
        try {
            this.substitution = new HashMap<>();
            this.dataWorker = dataWorker;
            this.tableName = tableName;
            synchronized (dataWorker) {
                setDataSource(dataWorker.getAllFromTable(tableName));
            }
            flag = true;

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();

        }

    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnName.size();
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnName.get(columnIndex);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (substitution.get(columnIndex) == null)
            return columnTypes.get(columnIndex);
        else {
            return String.class;
        }

    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (flag)
            return columnIndex != 0;
        else
            return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (substitution.get(columnIndex) == null)
            return data.get(rowIndex).get(columnIndex);
        else
            return substitution.get(columnIndex).get(new Integer(data.get(rowIndex).get(columnIndex).toString()));
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        synchronized (dataWorker) {
            if (dataWorker.update(tableName, "\"" + columnName.get(columnIndex) + "\"", "\'" + aValue.toString() + "\'", "\"" + columnName.get(0) + "\"", data.get(rowIndex).get(0).toString())) {
                data.get(rowIndex).set(columnIndex, aValue);
                this.fireTableRowsUpdated(rowIndex, rowIndex);
            }
        }
    }
    public String updateRow(String tablename, String column, String value, String key , String keyValue ){
        synchronized (dataWorker) {
            if (dataWorker.update(tablename, column, value, key, keyValue)) {
                updateTable(dataWorker.getData(request));
                fireTableDataChanged();
                return null;
            } else
                return "Неверный account";
        }
    }

    public String addRow(List<Object> newData) {
        String result;
        synchronized (dataWorker) {
        result= dataWorker.insert(tableName, newData);
        if (result == null) {

                updateTable(dataWorker.getAllFromTable(tableName));
                fireTableDataChanged();
            }
        }
        return result;
    }

    public String addRow(String req) {
        String result=null;
        synchronized (dataWorker) {
            result=dataWorker.getAnswer(req);
            updateTable(dataWorker.getData(request));
            fireTableDataChanged();
        }
        return result;
    }



    private void updateTable(ResultSet rs) {
        data = new ArrayList<>();
        try {
            while (true) {

                if (!rs.next()) break;

                ArrayList rowData = new ArrayList();
                for (int i = 0; i < columnName.size(); i++) {
                    if (columnTypes.get(i) == String.class)
                        rowData.add(rs.getString(i + 1));
                    else
                        rowData.add(rs.getObject(i + 1));
                }
                synchronized (data) {
                    data.add(rowData);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void setDataSource(ResultSet rs) throws SQLException, ClassNotFoundException {
        ResultSetMetaData rsmd = rs.getMetaData();

        int columnCount = rsmd.getColumnCount();
        for (int i = 0; i < columnCount; i++) {
            columnName.add(rsmd.getColumnName(i + 1));
            Class type = Class.forName(rsmd.getColumnClassName(i + 1));
            columnTypes.add(type);
        }
        updateTable(rs);
    }

    public void removeRow(int rowIndex) {
        synchronized (dataWorker) {
            dataWorker.delete(tableName, "\"" + columnName.get(0) + "\"", data.get(rowIndex).get(0).toString());
            updateTable(dataWorker.getAllFromTable(tableName));
            fireTableDataChanged();
        }
    }
    public void removeRow(int rowIndex, String tabN) {
        synchronized (dataWorker) {
            dataWorker.delete(tabN, "\"" + columnName.get(0) + "\"", data.get(rowIndex).get(0).toString());
            updateTable(dataWorker.getData(request));
            fireTableDataChanged();
        }

    }

    private void setSubstitution(int columnInd, Map<Integer, Object> map) {
        substitution.put(columnInd, map);
    }

    public void setSubstitution(int columnInd, String request) {
        synchronized (dataWorker) {
            setSubstitution(columnInd, dataWorker.parseData(dataWorker.getData(request)));
            fireTableStructureChanged();
        }
    }

    public void deleteColumn(int columnInd) {
        columnName.remove(columnInd);
        columnTypes.remove(columnInd);
        fireTableStructureChanged();
    }

    public Map<Integer, Object> getSubstitution(int columnInd) {
        return substitution.get(columnInd);
    }

    public void falseChange() {
        this.flag = false;
        fireTableStructureChanged();
    }
}
